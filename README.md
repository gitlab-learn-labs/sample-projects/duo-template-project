# Duo Template Project
Welcome to training with GitLab Duo! This project is the starting point for the ILT training GitLab Duo Principles. Here you'll work on the labs throughout the class.

# Additional Examples

If you complete the main labs [here](https://handbook.gitlab.com/handbook/customer-success/professional-services-engineering/education-services/devsecopswithduo/), you can try some additional examples.

## Additional Practice: 

- [Lab 1: Additional Questions](https://gitlab.com/gitlab-learn-labs/sample-projects/duo-template-project#lab-1)
- [Lab 2: Duo Chat and /fix Command](https://gitlab.com/gitlab-learn-labs/sample-projects/duo-template-project#lab-2)
- [Lab 3: Code Refactoring](https://gitlab.com/gitlab-learn-labs/sample-projects/duo-template-project#lab-3)
- [Lab 4: Code Suggestions](https://gitlab.com/gitlab-learn-labs/sample-projects/duo-template-project#lab-4)
- [Lab 5: Security Scan](https://gitlab.com/gitlab-learn-labs/sample-projects/duo-template-project#lab-5)

## Lab 1

In Lab 1, you created a new project and started asking GitLab Duo chat some questions. Here are some additional example questions you can try:

- What is a security policy in GitLab?
- What is the difference between a milestone and an epic?
- How do I set up a remote development environment in gitlab?
- What is puma in gitlab omnibus?
- Give me an example of building and testing a docker image in gitlab ci
- Give me an example of the workflow keyword that changes variable values based on git branch
- What level of group membership do you need to see group runners?

## Lab 2

Below are some additional prompts you can try. There are various files in this dir that are titled `explain.` that you can open and have Duo chat explain. 

#### Try the /fix command (we will demo this later too)

Paste the following in a new file in the WebIDE called `timezone.js`

```js
const moment = require('moment-timezone');

function getCityTimezone(city) {
  const cityTimezones = {
    'New York': 'America/New_York',
    'London': 'Europe/London',
    'Tokyo': 'Asia/Tokyo',
    'Sydney': 'Australia/Sydney',
    'Paris': 'Europe/Paris',
    // Add more cities and their corresponding timezones as needed
  };

  if (city in cityTimezone) {
    const timezone = cityTimezones[city];
    const currentTime = moment().tz(timezone).format('YYYY-MM-DD HH:mm:ss');
    return `The current time in ${city} (${timezone}) is: ${currentTime}`;
  } else {
    return `Sorry, we don't have timezone information for ${city}.`;
  }
}

// Example usage
console.log(getCityTimezone('New York'));
console.log(getCityTimezone('Tokyo'));
console.log(getCityTimezone('Unknown City')); 
```
Highlight the code and in the Duo chat window type `/fix`



#### Duo Chat Prompts

**Node**
- `How do I start a new node project?` 

**Example Response**:

Here are the steps to start a new Node.js project:

Create a new directory for your project:
`mkdir my-node-project`
`cd my-node-project`
Initialize npm (Node Package Manager):
`npm init`
This will create a package.json file with project info.

Install any dependencies you need:
`npm install express`
Create a main js file, like `index.js`, and require any modules:
```js
const express = require('express');
Write your Node.js code in the main file:
const app = express();

app.get('/', (req, res) => {
  res.send('Hello World'); 
});

app.listen(3000); 
```
Run the app:
`node index.js` 
The basic structure is now set up for a Node.js project using Express. You can add more files/dependencies as needed.

## Lab 3

In Lab 3 we created an issue, a merge request, and made some code changes. For additional practice try using GitLab Duo chat to `/refactor` the golang app we have built into another programming language, for example:

Ask duo: `How could I write the following in Javascript?`

Or `/refactor into Javascript`

## Lab 4

In Lab 4, we used code suggestions to create a new function and write new tests. Here are some additional examples you can try. 

A golang example: 

```go
// Create a Paid Time Off app for users
// Create a date-time picker 
// Provide start and end options
// Show public holidays based on the selected country
// Send the request to a server API
```

A JavaScript example: 

```js
// Create a Paid Time Off app for users
// Create a date-time picker in ReactJS
// Provide start and end options
// Show public holidays based on the selected country
// Send the request to a server API
```
A python example: 

```py
# Create a web service using Tornado that allows a user to log in, run a security scan, and review the scan results.
# Each action (log in, run a scan, and review results) should be its own resource in the web service
```

## Lab 5

If you would like to try another security scan with Duo's Explain the vulnerability, you can try the code below by adding a python file to your project.

```py
import subprocess

in = input("Enter your server ip: ")
subprocess.run(["ping", in])
```